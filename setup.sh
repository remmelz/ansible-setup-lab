#!/bin/bash

ctrlhost="control"
net="192."

[[ -d ./tmp ]] \
  && rm -rf ./tmp
mkdir ./tmp

#####################################################

while read line; do
  ipaddr=`  echo ${line} | awk -F' ' '{print $1}'`
  hostname=`echo ${line} | awk -F' ' '{print $2}'`

  [[ -n `echo ${hostname} | grep localhost` ]] && continue
  [[ -z ${ipaddr} ]] && continue

  ping -c1 ${ipaddr}
  if [[ $? != 0 ]]; then
    echo "Error: could not reach host ${hostname}"
    exit 1
  fi

  if [[ -n `echo ${hostname} | grep ${ctrlhost}` ]]; then
    ssh-copy-id root@${ipaddr}
  fi

done < hosts

#####################################################

c=0
while read line; do

  ipaddr=`  echo ${line} | awk -F' ' '{print $1}'`
  hostname=`echo ${line} | awk -F' ' '{print $2}'`

  [[ -n `echo ${hostname} | grep localhost` ]] && continue
  [[ -z ${ipaddr} ]] && continue

  group=`echo ${hostname} | sed "s/[0-9]//" | awk -F'.' '{print $1}'`
  echo "${hostname} ansible_host=${ipaddr}" >> ./tmp/${group}

done < hosts

#####################################################

for grp in `ls ./tmp`; do
  echo '['${grp}']' >> ./tmp/inventory
  cat ./tmp/${grp}  >> ./tmp/inventory
  echo              >> ./tmp/inventory
done

cat login >> ./tmp/inventory

ansible-playbook --inventory-file ./tmp/inventory playbook.yml

rm -rf ./tmp

